require('./conf');
const express    = require('express');
const bodyParser = require('body-parser');
const routes     = require('./example.route').Route;

let app = express();

routes.start(app);

app.use(bodyParser.json());
app.use('/doc', express.static('./doc'));

app.listen(process.env.port);
console.log(`app runs on ${process.env.host}:${process.env.port}`);