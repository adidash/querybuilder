const DbPool = require('./tool.dbpool').dbpool;

/**
 * @description
 * This allows to build sql query and execute it synchronously or not.</br>
 * The class use a pool, to get another pool, make another instance of this class.
 */
class QueryBuilder extends DbPool{

    /**
     * @param {Object} dto
     * @param {string} dto.dbName - the database Name
     * @param {string} dto.dbUser - the database user
     * @param {string} dto.dbHost - the database host
     * @param {string} dto.dbPort - the database port
     * @param {string} dto.dbPassword - the database password
     */
    constructor(dto){
        super(dto);
        this.query = '';
    }

    //region UTILS
    /**
     * @description
     * This transforms a string,boolean or number into an array.</br>
     * In the case of the param is already an array, it will be return as is.
     *
     * @param {Array | string | boolean | number} stringToArrayify - the param to format into an array.
     * @return {Array}
     */
    arrayify(stringToArrayify){
        return (Array.isArray(stringToArrayify)) ? stringToArrayify : [stringToArrayify];
    }

    /**
     * @description
     * This allows to remove unused characters from the query.</br>
     * if the length param is not defined, it will remove only the latest char.</br>
     * else it will remove the latest #length characters.
     *
     * @param {number=} length - the number of characters to remove from the query.
     */
    removeUnused(length){
        if(length){
            let queryLength = this.query.length - length;
            this.setSql(this.query.substr(0, queryLength), true);
        } else {
            this.setSql(this.query.substr(0, this.query.length -1), true);
        }
    }
    //endregion


    //region METHODS
    /**
     * @description
     * Add or set query string.
     *
     * @param {string} sql - the query to add or set.
     * @param {boolean} [replace=false] - replace the query if true, add the sql string to query if not.
     */
    setSql(sql, replace=false){
        if(!replace) this.query += sql;
        else this.query = sql;
    }

    /**
     * @description
     * This allows to get the sql query.</br>
     * Warning: </br>
     *  - this adds a ';' (semicolon) to the query.</br>
     *  - if <empty> it is true, this cleans the query builder.
     *
     * @param {boolean} [empty=true] - empty the query memory if true.
     *
     * @return {string} query - the string query.
     */
    getSql(empty=true){
        this.setSql(";");
        let query = this.query;

        if(empty) this.setSql("", true);

        return query;
    }
    //endregion


    //region FUNCTIONS
    /**
     * @description
     * This builds the SELECT #items into query.
     *
     * @param {Array | string} items - The array of items to select. this can be a string id unique item.</br>
     * you can use alias like: ["attribute AS att", ...]
     *
     * @return {QueryBuilder}
     */
    select(items){
        items = this.arrayify(items);
        this.setSql("SELECT ");

        items.forEach( item => this.setSql(` ${item},`));

        this.removeUnused();

        return this;
    }

    /**
     * @description
     * This builds the DELETE into query.
     *
     * @return {QueryBuilder}
     */
    delete(){
        this.setSql(`DELETE `);

        return this;
    }

    /**
     * @description
     * This builds the INSERT INTO #tables into query.
     *
     * @param {string} table - the table to insert.
     * @param {Array | string} [columns=null] - The array of columns to select. this can be a string id unique column.
     *
     * @return {QueryBuilder}
     */
    insert(table, columns=null){
        this.setSql(`INSERT INTO ${table}`);

        if(columns){

            columns = this.arrayify(columns);
            this.setSql(` (`);
            columns.forEach( column => this.setSql(`"${column}",`) );
            this.removeUnused();
            this.setSql(` )`);
        }

        return this;
    }

    /**
     * @description
     * This builds the UPDATE #tables into query.
     *
     * @param {string} table - the table to insert.
     *
     * @returns {QueryBuilder}
     */
    update(table){
        this.setSql(`UPDATE ${table}`);

        return this;
    }

    /**
     * @description
     * This builds the #subQueryType into query.
     *
     * @param {string} [subQueryType=EXISTS] - the subQuery type (at the moment 'EXISTS', IN', 'NOT IN')
     *
     * @returns {QueryBuilder | boolean}
     */
    startSubQuery(subQueryType="EXISTS"){

        // if subQueryType is not one of these type an Error will be thrown
        if(subQueryType !== "NOT IN" &&
            subQueryType !== "EXISTS" &&
            subQueryType !== "IN"){

            throw new Error(`
            ==================================================================================
            In the query:
            ${this.query} ${subQueryType} 
            
            you are trying to build a subQuery with another type than:
                
                - EXISTS
                - IN
                - NOT IN
                
            All other types are not supported yet.
            ===================================================================================
            `)
        }

        this.setSql(` ${subQueryType} (`);

        return this;
    }

    /**
     * @description
     * This builds the ) of the end of subQuery into query.
     *
     * @returns {QueryBuilder | boolean}
     */
    endSubQuery(){
        this.setSql(" )");

        return this;
    }

    /**
     * @description
     * This builds the FROM #tables into query.
     *
     * @param {Array | string} tables - the array of tables to insert. This can be a string unique table.</br>
     * the tables can be used with alias like: ['myTable as mt', ...]
     *
     * @returns {QueryBuilder}
     */
    from(tables){
        tables = this.arrayify(tables);

        this.setSql(" FROM");

        tables.forEach( table => this.setSql(` ${table},`));

        this.removeUnused();

        return this;
    }

    /**
     * @description
     * This builds the WHERE #condition into query.
     *
     * @param {string | object} [condition=''] - the string or object of condition(s) to insert.</br>
     * you can use tableName or alias like: condition = "myTable.attribute" or "mt.attribute"</br>
     *
     * example:</br>
     *  - condition = {"firstname": 'foo', "age": [25, 26]}
     *  - condition = "firstname"
     *  - condition = "person.firstname"
     *
     * @returns {QueryBuilder}
    */
    where(condition){

        if(typeof condition === "string" || condition === null){

            this.setSql(` WHERE ${condition}`);

        } else if(typeof condition === "object"){

            Object.keys(condition).forEach( (key, idx) => {

                let value = condition[key];
                if(idx === 0)
                    this.setSql(` WHERE ${key}`);
                else
                    this.setSql(` AND ${key}`);

                if(Array.isArray(value))
                    this.in(value);
                else
                    this.equal(value);
            });
        }

        return this;
    }

    /**
     * @description
     * This builds the AND #condition into query.
     *
     * @param {string} condition - the string of condition to insert.s
     *
     * @returns {QueryBuilder}
     */
    and(condition){
        this.setSql(` AND ${condition}`);

        return this;
    }

    /**
     * @description
     * This builds the = #value into query.
     *
     * @param {string} value - the string of value to insert.
     *
     * @returns {QueryBuilder}
     */
    equal(value){
        this.setSql(` = '${value}'`);

        return this;
    }

    /**
     * @description
     * This builds the IN ( #values ) into query.
     *
     * @param {Array | string} values - the array of values to insert. This can be a single string.
     *
     * @returns {QueryBuilder}
     */
    in(values){
        values = this.arrayify(values);

        this.setSql(" IN ( ");

        values.forEach( value => this.setSql(` '${value}',`) );
        this.removeUnused();
        this.setSql(" )");

        return this;
    }

    /**
     * @description
     * This builds the VALUES ( #values ) into query.
     *
     * @param {Array | string} values - the array of values to insert. This can be a single string.
     *
     * @returns {QueryBuilder}
     */
    values(values){
        values =this.arrayify(values);
        this.setSql(` VALUES (`);

        values.forEach( value => this.setSql(`'${value}',`) );
        this.removeUnused();
        this.setSql(` )`);

        return this;
    }

    /**
     * @description
     * This builds the SET "#Object.key" = '#Object.value'  into query.
     *
     * @param {Object} objectOfConditions - the object of values to set.
     *
     * @returns {QueryBuilder}
     */
    set(objectOfConditions){
        this.setSql(` SET `);

        Object.keys(objectOfConditions).forEach( key => this.setSql(`"${key}" = '${objectOfConditions[key]}',`) );
        this.removeUnused();

        return this;
    }

    /**
     * @description
     * This builds the HAVING #condition into query.
     *
     * @param {string} condition - The entire condition of HAVING.</br>
     * should looks like:
     * - condition = "HAVING COUNT(Id) > 10"
     * </br>
     * I'll make it better later without breaking this code (to be sure that updating will not make your app crash)
     *
     * @returns {QueryBuilder}
     */
    having(condition){
        this.setSql(` HAVING ${condition}`);

        return this;
    }

    /**
     * @description
     * This builds the GROUP BY #columnNames into query.
     *
     * @param {Array | string} columnNames - The string or array of column names
     *
     * @returns {QueryBuilder}
     */
    groupBy(columnNames){
        columnNames = this.arrayify(columnNames);

        this.setSql(` GROUP BY `);

        columnNames.forEach( columnName => this.setSql(` ${columnName},`) );
        this.removeUnused();

        return this;
    }
    //endregion


    //region EXECUTING FUNCTIONS
    /**
     * @description
     * This allows to execute the query.
     *
     * @returns {Promise}
     */
    execute(){
        return this.launchAsynchQuery(this.getSql());
    }

    /**
     * @description
     * This allows to execute synchronously the query.
     *
     * @returns {Promise} - the awaited "promise" result (data or error)
     */
    async syncExecute(){
        return await this.launchSyncQuery(this.getSql());
    }
    //endregion

}

module.exports = QueryBuilder;