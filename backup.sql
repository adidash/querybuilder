PGDMP                 
    	    u           postgres    9.6.5    10.0     O           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            P           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            Q           1262    12401    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE postgres;
             postgres    false            R           1262    12401    postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                  postgres    false    2129                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            S           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    4                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            T           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    2                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                  false            U           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                       false    1            �            1259    24578    person    TABLE     �   CREATE TABLE person (
    id bigint NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL,
    age text NOT NULL,
    sex boolean NOT NULL
);
    DROP TABLE public.person;
       public         postgres    false    4            �            1259    24576    Person_id_seq    SEQUENCE     q   CREATE SEQUENCE "Person_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public."Person_id_seq";
       public       postgres    false    4    187            V           0    0    Person_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE "Person_id_seq" OWNED BY person.id;
            public       postgres    false    186            �           2604    24581 	   person id    DEFAULT     Z   ALTER TABLE ONLY person ALTER COLUMN id SET DEFAULT nextval('"Person_id_seq"'::regclass);
 8   ALTER TABLE public.person ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    187    186    187            L          0    24578    person 
   TABLE DATA               <   COPY person (id, firstname, lastname, age, sex) FROM stdin;
    public       postgres    false    187   �       W           0    0    Person_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('"Person_id_seq"', 6, true);
            public       postgres    false    186            �           2606    24586    person Person_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY person
    ADD CONSTRAINT "Person_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.person DROP CONSTRAINT "Person_pkey";
       public         postgres    false    187            L   I   x�3�,�/�7�,�,�4�42�,�2�E8�L9ӸL�L93Rsr�E�8KRKRSB@*al�����=... l�m     